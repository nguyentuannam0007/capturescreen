package de.vogella.android.democanvas;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlendMode;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.drawable.GradientDrawable;
import android.hardware.display.DisplayManager;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class MainActivity extends AppCompatActivity implements ImageReader.OnImageAvailableListener {

    public static int CAPTURE_SCREEN_REQUEST = 1999;
    Button btnCaptureScreen;
    Button btnSeeImage;
    Button btnStopCapture;

    MediaProjectionManager projectionManager;
    MediaProjection projection;
    Handler handler;
    ImageReader imageReader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCaptureScreen = findViewById(R.id.capture_screen);
        btnSeeImage = findViewById(R.id.see_image);
        btnStopCapture = findViewById(R.id.stop_capture);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                handler = new Handler();
                Looper.loop();
            }
        }).start();

        projectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);

        /*
            Đôi chút nhận xét về sử dụng ProjectionManager
                Muốn tạo được Projection thì phải có resultCode, data (Intent)
                Mỗi lần sử dụng phải tạo ra cái intent để lấy được resultCode và data. ProjectionManger làm việc đó cho mình.
         */

        btnCaptureScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(projectionManager.createScreenCaptureIntent(), CAPTURE_SCREEN_REQUEST);
            }
        });

        btnSeeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File imageF = new File(getExternalFilesDir("images"), "screen_shot.png");
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                i.setDataAndType(FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", imageF), "image/*");
                i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(i);
            }
        });

        btnStopCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (projection != null){
                    projection.stop();
                } else {
                    Log.i("EEE", "Projection is null");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CAPTURE_SCREEN_REQUEST){
            if (resultCode == RESULT_OK){
                projection = projectionManager.getMediaProjection(resultCode, data);
                startCaptureScreen();
            }
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    private void startCaptureScreen(){
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Pair<Integer, Integer> dimen = getDimenDevice();

        imageReader = ImageReader.newInstance(dimen.first, dimen.second, PixelFormat.RGBA_8888, 2);

        imageReader.setOnImageAvailableListener(this, handler);

        // Goi phuong thuc nay chinh la goi capture screen nha. No tu dong render anh len surface minh truyen vao
        projection.createVirtualDisplay("SCREEN_SHOT", imageReader.getWidth(), imageReader.getHeight(),
                metrics.densityDpi, DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY | DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC, imageReader.getSurface(), null, handler);
        projection.registerCallback(new MediaProjection.Callback() {
            @Override
            public void onStop() {
                super.onStop();
                projection.unregisterCallback(this);
            }
        }, handler);
    }

    public Pair<Integer, Integer> getDimenDevice(){
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        int navigationH = getNavigationBarHeight(Configuration.ORIENTATION_PORTRAIT);
        if (navigationH > 0){
            height += navigationH;
        }

        return new Pair<>(width, height);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private int getNavigationBarHeight(int orientation) {
        Resources resources = getResources();
        int id = resources.getIdentifier(
                orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_height_landscape",
                "dimen", "android");
        if (id > 0) {
            return resources.getDimensionPixelSize(id);
        }
        return 0;
    }

    @Override
    public void onImageAvailable(ImageReader reader) { // Phan nay dung de luu anh khi capture xong
        projection.stop();
        Toast.makeText(this, "Screen Capture has done !", Toast.LENGTH_SHORT).show();
        Image image = reader.acquireLatestImage();
        if (image != null){
            Pair<Integer, Integer> p = getDimenDevice();


            Image.Plane[] planes = image.getPlanes();
            ByteBuffer byteBuffers = planes[0].getBuffer();

            // Doan nay dung de cau hinh do rong cua bitmap de chua anh vua chup
            int pixelStride = planes[0].getPixelStride();
            int rowStride = planes[0].getRowStride();
            int rowPadding = rowStride - pixelStride * p.first;
            int bitmapWidth = p.first + rowPadding / pixelStride;

            Bitmap bitmap = Bitmap.createBitmap(bitmapWidth, p.second, Bitmap.Config.ARGB_8888);
            bitmap.copyPixelsFromBuffer(byteBuffers);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            File file = new File(getExternalFilesDir("images"), "screen_shot.png");

            try {
                FileOutputStream out = new FileOutputStream(file);
                out.write(baos.toByteArray());
                out.getFD().sync();
                out.close();
            } catch (IOException e){
                e.printStackTrace();
            }

            image.close();
        }
    }
}